package Tests;

import Data.LoadProperties;
import Pages.*;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.*;
import java.time.Duration;

import static Tests.EncryptionToolsTest.passwordQA;
import static Tests.EncryptionToolsTest.usernameQA;

public class PDFProtectorTest extends TestBase {
    static HomePage homeobject;
    static LoginPage loginobject;
    static WebDriverWait wait;
    static EncryptionToolsPage encryptionToolsPage;
    static ImageprotectorPage imageprotectorPage;
    static PDFProtectorPage pdfProtectorPage;
    static  PageBase page;
    public  static String usernameQA1 = LoadProperties.testData.getProperty("usernameQALocalKey");
    public  static String passwordQA2 = LoadProperties.testData.getProperty("passwordQALocalKey");
    String originPDF = LoadProperties.testData.getProperty("PDFPATH");
    String encryptedselectionPDF = LoadProperties.testData.getProperty("encryptedselectionPDF");
    static String selectionText;
    static String tracerIDselectionPDF;
    public PDFProtectorTest() throws AWTException {
    }
    @Test
    public void successful_FindandEncrypt_encryption_PDF() throws AWTException, InterruptedException {
        homeobject = new HomePage(driver);
        homeobject.individual_login();
        loginobject = new LoginPage(driver);
        page = new PageBase(driver);
        loginobject.LOGIN(usernameQA1, passwordQA2);
        pdfProtectorPage = new PDFProtectorPage(driver);
        pdfProtectorPage.PDFprotector.click();
       imageprotectorPage = new ImageprotectorPage(driver);
        imageprotectorPage.choosefilebtn.sendKeys(originPDF);
        wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        wait.until(ExpectedConditions.visibilityOf(imageprotectorPage.encryptWholeBtn));
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("arguments[0].click();", pdfProtectorPage.dropdownBTN);
        selectionText = "ingy";
        pdfProtectorPage.Find_encryptPDF(selectionText , driver);
        driver.switchTo().frame(0);
        pdfProtectorPage.Savebtn_pdf.click();
        Thread.sleep(5000);
        driver.switchTo().defaultContent();
        //page.save_files(encryptedselectionPDF);
        Assert.assertTrue(imageprotectorPage.viewencryptedBTN.isEnabled());
        //should talk with the team about makeing this button disabled.
        //Assert.assertFalse(imageprotectorPage.encryptDisabledbtn.isEnabled());
        Assert.assertTrue(imageprotectorPage.removeallencryptionBTN.isEnabled());
        Assert.assertFalse(imageprotectorPage.removeSelectedbtn.isEnabled());
        imageprotectorPage.editBtn2.click();
        WebElement tracerid = imageprotectorPage.tracerDetails.get(0);
        tracerIDselectionPDF = tracerid.getText();
        System.out.print(" this is tracer for Find and encrypt pdf ");
        System.out.print(tracerIDselectionPDF);
        Assert.assertTrue(imageprotectorPage.tracerName.isDisplayed());
    }
}
