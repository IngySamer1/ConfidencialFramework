package Pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.time.Duration;
import java.util.List;

public class PDFProtectorPage extends PageBase {

    public PDFProtectorPage(WebDriver driver) throws AWTException {
        super(driver);
    }
    @FindBy(xpath = "//*[@id=\"container\"]/div/div[2]/div/div[3]/div/div[1]/div[1]/div[2]/div[2]")
    public static WebElement PDFprotector;
    @FindBy(xpath = "//*[@id=\"container\"]/div/div[2]/div/div[3]/div/div[2]/div/div/div/div[1]/div/input")
    public static WebElement PDFarea;
    @FindBy(xpath = "//button[@class='SearchResult ']")
    public static List<WebElement> searchResults;
    @FindBy(id = "save-button")
    public static WebElement Savebtn_pdf;
    @FindBy(xpath = "//*[@id=\"app\"]/div[1]/div[1]/div[1]/div[5]")
    public static WebElement opeenn;

    @FindBy(xpath = "//button[@class='rounded select-none bg-white border border-gray-400 px-3 py-2.5 font-semibold text-lg hover:bg-gray-100 cursor-pointer' and @type='button']")
    public
    WebElement dropdownBTN;
    @FindBy(xpath = "//button[@class='false group flex rounded-b-md items-center w-full px-5 py-2 text-c11-darkblue']")
    public static List<WebElement> selectionModes;

    @FindBy(id ="SearchPanel__input")
    WebElement inputSearchtext;
    static WebDriverWait wait;

    public void Find_encryptPDF(String text , WebDriver driver) throws InterruptedException, AWTException {
        selectionModes.get(3).click();
        driver.switchTo().frame(0);
        inputSearchtext.sendKeys(text);
        searchResults.get(0).click();
        driver.switchTo().defaultContent();
        ImageprotectorPage.encryptWholeBtn.click();
        wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        wait.until(ExpectedConditions.visibilityOf(ImageprotectorPage.viewencryptedBTN));
    }
}
